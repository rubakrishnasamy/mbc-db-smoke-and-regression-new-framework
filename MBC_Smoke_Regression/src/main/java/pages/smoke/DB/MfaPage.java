package pages.smoke.DB;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.switchToWindowByTitle;
import static driverfactory.Driver.waitForElementToDisplay;

import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.InitTests;

public class MfaPage extends InitTests {
	WebDriver driver;
	static String Code;
	String outlookurl = "https://apac1mail.mmc.com/OWA/";

	static String browser = InitTests.BROWSER_TYPE;

	public MfaPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@FindBy(xpath = "//h2[contains(text(),'almost there')]")
	public WebElement mfapageheader;

	@FindBy(css = "input[id*='username']")
	public static WebElement outlookemailid;

	@FindBy(css = "input[id='password']")
	public static WebElement OutlookPassword;

	@FindBy(css = "div[class*='signinbutton']")
	public WebElement signInBtn;

	@FindBy(xpath = "//label[contains(text(),'Email ru*****@mercer.com')]")
	public WebElement selectcontact;

	@FindBy(xpath = "//button[contains(text(),'Continue')]")
	public WebElement sendContBtn;

	@FindBy(css = "input[id='verificationCode']")
	public WebElement enterCodeTxt;

	@FindBy(css = "input[name*='proceed']")
	public WebElement confirmCodeBtn;

	@FindBy(xpath = "(//span[@class='_n_Z6 ms-font-s ms-font-weight-semibold'])[1]")
	public WebElement Mfa_InboxFolder;

	@FindBy(xpath = "//span[@class='_n_Z6 ms-font-s ms-font-weight-semibold' and contains(text(),'MFA')]")
	public WebElement mfa_Folder;

	// @FindBy(xpath = "//span[contains(text(),'Verification code')]")
	@FindBy(xpath = "(//span[@class='lvHighlightAllClass lvHighlightBodyClass'])[1]")
	public WebElement Mfa_Mail;

	@FindBy(xpath = "(//div[contains(string(),'Administrative bypass of MFA Service')])[8]")
	public WebElement errorMessage;

	@FindBy(xpath = "(//a[@id='mfa-choose-another-method'])[1]")
	public WebElement resendOTP;

	public void authenticate() throws InterruptedException {
		Thread.sleep(10000);

		waitForElementToDisplay(selectcontact);
		clickElementUsingJavaScript(driver, selectcontact);
		System.out.println(driver.getTitle());
		clickElement(sendContBtn);
		fetchMFACodeFromOutlookEmail(outlookurl);
		waitForElementToDisplay(enterCodeTxt);
		setInput(enterCodeTxt, Code);
		waitForElementToDisplay(sendContBtn);
		clickElement(sendContBtn);
		/*
		 * if(errorMessage.isDisplayed()==true) { if(selectcontact.isDisplayed()==true)
		 * { fetchMFACodeFromOutlookEmail(outlookurl);
		 * waitForElementToDisplay(enterCodeTxt); setInput(enterCodeTxt,Code);
		 * waitForElementToDisplay(confirmCodeBtn); clickElement(confirmCodeBtn);
		 * 
		 * fetchMFACodeFromOutlookEmail(outlookurl);
		 * waitForElementToDisplay(enterCodeTxt); setInput(enterCodeTxt,Code);
		 * waitForElementToDisplay(confirmCodeBtn); clickElement(confirmCodeBtn); }else
		 * { clickElement(resendOTP); fetchMFACodeFromOutlookEmail(outlookurl);
		 * waitForElementToDisplay(enterCodeTxt); setInput(enterCodeTxt,Code);
		 * waitForElementToDisplay(confirmCodeBtn); clickElement(confirmCodeBtn); }
		 * }else {
		 * 
		 * System.out.println("Successful");
		 * 
		 * }
		 */
	}

	public void fetchMFACodeFromOutlookEmail(String url) throws InterruptedException {

		((JavascriptExecutor) driver).executeScript("window.open()");
		ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.manage().window().maximize();
		driver.get(url);
		waitForElementToDisplay(outlookemailid);
		outlookemailid.click();
		outlookemailid.clear();
		// outlookemailid.sendKeys(MFAEmailId);
		setInput(outlookemailid, MFAEmailId);
		waitForElementToDisplay(OutlookPassword);
		OutlookPassword.click();
		OutlookPassword.clear();
		setInput(OutlookPassword, MFAEmailPassword);
		clickElement(signInBtn);
		waitForElementToDisplay(Mfa_InboxFolder);
		clickElementUsingJavaScript(driver, Mfa_InboxFolder);
		//clickElementUsingJavaScript(driver, mfa_Folder);
		scrollToElement(driver, Mfa_Mail);

		clickElementUsingJavaScript(driver, Mfa_Mail);

		Code = Mfa_Mail.getText();
		Code = Code.substring(27, 33);
		System.out.println(Code);
		driver.close();
		switchToWindowByTitle("Mercer BenefitsCentral", driver);
		/*
		 * if(browser.contains("HEADLESSGC")) { waitForElementToDisplay(masAppMode);
		 * clickElement(masAppMode); clickElement(verificationcode); Code =
		 * selectmfa.getText(); Code = Code.substring(27, 33); driver.close();
		 * switchToWindowByTitle("iBenefit Center",driver); }
		 */
	}

}
