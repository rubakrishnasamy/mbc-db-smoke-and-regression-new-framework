package pages.smoke.DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ROL_Page {
	WebDriver driver;
	public ROL_Page(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	
	//------------------------------------------FP Elements------------------------------------------//	
	
	@FindBy(xpath = "//h2[contains(text(),'Benefit Commencement')]")
	public WebElement BenefitCommencementLabel;
	
	@FindBy(xpath = "//h2[contains(text(),'Benefit Commencement Overview')]")
	public WebElement BenefitCommOverviewLabel;
	
	@FindBy(css = "button[ng-click*='startCommencement']")
	public WebElement GetStartedROLBtn;
	
	//------------------------------------------ Elements------------------------------------------//
	
	@FindBy(xpath = "//span[contains(text(),'Status Details')]")
	public WebElement StatusDetailsLink;
	
	@FindBy(css = "h2[data-bo-bind='pageHeader']")
	public WebElement BenCommStepsLabel;
	
	@FindBy(css = "div.gray-back > div:nth-child(1) > table > tbody > tr > td:nth-child(1)")
	public WebElement StepOneLabel;
	
	@FindBy(css = "div.gray-back > div:nth-child(2) > table > tbody > tr > td:nth-child(1)")
	public WebElement StepTwoLabel;
	
	@FindBy(css = "div.gray-back > div:nth-child(3) > table > tbody > tr > td:nth-child(1)")
	public WebElement StepThreeLabel;
	
	@FindBy(css = "a[ng-click='backToOverview()']")
	public WebElement BackOVBtn;
	
	@FindBy(xpath = "//h2[contains(text(),'Request a Benefit Commencement Kit')]")
	public WebElement ReqBenCommKitLabel;
	
	@FindBy(xpath = "//span[contains(text(),'Get Started')]")
	public WebElement GetStartedRKBtn;
	
	//------------------------------------------ROL Methods------------------------------------------//
	
	public int StatusStep() throws InterruptedException {
		DashboardPageLR dashboard = new DashboardPageLR(driver);
		String Act = dashboard.StatusLabel.getText();
		System.out.println(Act);
		String[] curStep = Act.split(" ");
		System.out.println(curStep[1]);
		return Integer.parseInt(curStep[1]);
	}
}
