package pages.smoke.DB;

import static driverfactory.Driver.clickElement;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashboardPageLR {
	WebDriver driver;

	public DashboardPageLR(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	
	//------------------------------------------General Elements------------------------------------------//
	
	// @FindBy(xpath = "//span[contains(text(),'Dashboard')]")
	@FindBy(css = "span[data-bo-bind*='Dashboard']")
	public WebElement DashboardTab;

	// @FindBy(xpath = "//span[contains(text(),'Benefits Summary')]")
	@FindBy(css = "span[data-bo-bind*='BenefitsSummary']")
	public WebElement BenefitsSummaryTab;

	@FindBy(xpath = "//span[contains(text(),'RETIREMENT PENSION')]")
	public WebElement RetirementPensionTitle;

	// @FindBy(xpath = "//span[contains(text(),'Benefit Commencement')]")
	@FindBy(css = "span[data-bo-bind*='BenefitCommencement']")
	public WebElement BenefitCommencementTile;
	
	@FindBy(xpath = "//span[contains(text(),'RETIREMENT PLANNING')]")
	public WebElement RetirementPlanningTile;

	@FindBy(css = "a[title='Estimate now']")
	public WebElement EstimateNowBtn;

	@FindBy(css = "span[mbc-content-i*='RetirementTitle']")
	public WebElement RetirementLabel;

	// @FindBy(xpath = "//span[contains(text(),'Menu')]")
	@FindBy(css = "span[data-bo-bind*='Menu']")
	public WebElement MenuBtn;

	// @FindBy(css = "a[href*='dashboard']")
	@FindBy(xpath = "//span[contains(text(),'Home')]")
	public WebElement HomeBtn;

	@FindBy(css = "a[href*='my-account/personal-information']")
	public WebElement EditProfileLink;

	@FindBy(xpath = "//h6[contains(text(),'My Account')]")
	public WebElement MyAccountLabel;

	@FindBy(xpath = "//span[contains(text(),'Profile')]")
	public WebElement ProfileLink;

	@FindBy(xpath = "//span[contains(text(),'Communications')]")
	public WebElement CommunicationsLink;

	@FindBy(xpath = "//span[contains(text(),'Beneficiaries')]")
	public WebElement BeneficiariesLink;
	
	@FindBy(xpath = "//h6[contains(text(),'Retirement Pension')]")
	public WebElement RetirementPensionLabel;
	
	@FindBy(xpath = "//h6[contains(text(),'Forms & Documents')]")
	public WebElement Forms_DocLabel;

	@FindBy(xpath = "//span[contains(text(),'Retirement Pension')]")
	public WebElement Retirement_PensionLink;

	@FindBy(xpath = "//h6[contains(text(),'Knowledge Center')]")
	public WebElement KnowledgeCenterLabel;

	@FindBy(xpath = "//span[contains(text(),'Retirement')]")
	public WebElement RetirementLink;

	@FindBy(xpath = "//span[contains(text(),'Money Management')]")
	public WebElement MoneyManagementLink;

	@FindBy(xpath = "//h6[contains(text(),'UPLOAD DOCUMENTS')]")
	public WebElement UploadDocsLabel;

	@FindBy(xpath = "//span[contains(text(),'General Documents')]")
	public WebElement GeneralDocumentsLink;

	@FindBy(xpath = "//span[contains(text(),'Overview')]")
	public WebElement OverviewLink_hm;
	
	@FindBy(xpath = "//span[contains(text(),'Tax Withholding')]")
	public WebElement TaxWithHoldinglink_hm;
	
	@FindBy(xpath = "//span[contains(text(),'Payment Method')]")
	public WebElement PaymentMethodlink_hm;
	
	
	@FindBy(xpath = "//a[contains(text(),'Logout')]")
	public WebElement logoutBtn;
	
	
	//-----------------------------------------Multi-Plan Elements---------------------------------------------//
	
	@FindBy(css = "div[class*='btns-group'] > button[ng-click='closePopup(false)']")
	public WebElement PopOKBtn;
	
	@FindBy(xpath = "//h2[contains(text(),'Multi-Plan Estimator Tool')]")
	public WebElement MPEstimatorLabel;
	
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(1) > a > span.ng-binding")
	public WebElement EstYourMPPensionBenTab;
	
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(2) > a > span.ng-binding")
	public WebElement MstRcntMPEstTab;
	
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(3) > a > span.ng-binding")
	public WebElement SavedMPEstTab;
	
	@FindBy(xpath = "//span[contains(text(),'Estimate Your Multi-Plan Pension Benefits')]")
	public WebElement EstYourMPPensionBenLabel;
	
	@FindBy(xpath = "//span[contains(text(),'Multi-Plan Estimate Results')]")
	public WebElement MPEstResultsLabel;
	
	@FindBy(xpath = "//span[contains(text(),'Saved Multi-Plan Estimates')]")
	public WebElement SavedMPEstLabel;
	
	@FindBy(xpath = "//span[contains(text(),'Multi-Plan Estimator Tool')]")
	public WebElement MPEstimatorTile;
	
	
	//------------------------------------------ROL Elements------------------------------------------//
	@FindBy(xpath = "//span[contains(text(),'Not Started')]")
	public WebElement NotStartedLabel;
	
	//@FindBy(xpath = "span[contains(text(),'Get Started')]")
	@FindBy(css = "button[ng-click*='getStarted()']")
	public WebElement GetStartedBtn;
	
	@FindBy(css = "button[ng-click*='resume']")
	public WebElement ResumeBtn;
	
	@FindBy(css = "a[ng-click*='Delete']")
	public WebElement CancelCommLink;
	
	@FindBy(css = "div.status-info > span.status-progress.ng-scope >span")
	public WebElement StatusLabel;
	
	
	//------------------------------------------BW Avon Elements------------------------------------------//
	@FindBy(xpath = "//span[contains(text(),'ADP Voluntary Early Retirement Program')]")
	public WebElement ADPTile;
		
	@FindBy(xpath = "//span[contains(text(),'My Elections')]")
	public WebElement MyElectionsLink;
	
	@FindBy(css = "a[data-ng-href='/db-plan/summary/AVON']")
	public WebElement PlanOneLinkHm;
	
	@FindBy(css = "a[data-ng-href='/db-plan/summary/AVONNQ']")
	public WebElement PlanTwoLinkHm;
	
	//------------------------------------------Dashboard Methods----------------------------------------------//	

	public void logoutDB() throws InterruptedException {
		clickElement(logoutBtn);
	}

}
