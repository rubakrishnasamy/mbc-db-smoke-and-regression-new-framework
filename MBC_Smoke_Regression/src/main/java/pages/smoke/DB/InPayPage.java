package pages.smoke.DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class InPayPage {
	WebDriver driver;

	public InPayPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}
	// ----------------------------------------Inpay Elements------------------------------------------//

		@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(2) > a")
		public WebElement TaxWithHoldingTab;

		@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(3) > a")
		public WebElement PaymentMethodTab;

		@FindBy(xpath = "//a[text()='Summary']")
		public WebElement SummaryLink;

		@FindBy(xpath = "//a[text()='Payment History']")
		public WebElement PaymentHistoryLink;

		@FindBy(xpath = "//a[text()='Scheduled Deductions']")
		public WebElement ScheduledDeductionsLink;

		@FindBy(xpath = "//span[contains(text(),'Payment Summary')]")
		public WebElement PaymentSummaryLabel;

		@FindBy(xpath = "//span[contains(text(),'Payment History')]")
		public WebElement PaymentHistoryLabel;

		@FindBy(xpath = "//h2[contains(text(),'Scheduled Deductions')]")
		public WebElement ScheduledDeductionsLabel;

		@FindBy(xpath = "//a[text()='Federal']")
		public WebElement FederalLink;

		@FindBy(xpath = "//a[text()='State']")
		public WebElement StateLink;

		@FindBy(xpath = "//span[contains(text(),'Federal Tax Withholding')]")
		public WebElement FederalTaxWithHoldingLabel;

		@FindBy(xpath = "//span[contains(text(),'State Tax Withholding')]")
		public WebElement StateTaxWithHoldingLabel;

		@FindBy(xpath = "//h2[contains(text(),'Payment Method')]")
		public WebElement PaymentMethodLabel;
		
}
