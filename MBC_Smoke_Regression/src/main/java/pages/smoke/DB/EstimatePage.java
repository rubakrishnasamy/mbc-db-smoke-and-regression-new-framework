package pages.smoke.DB;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;


public class EstimatePage {
	WebDriver driver;
	public EstimatePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	
	@FindBy(xpath="//span[contains(text(),'Overview')]")
	public WebElement OverviewHRtab;
	
	/*@FindBy(xpath="//span[contains(text(),'Estimates')]")
	public WebElement EstimatesTab;
	
	@FindBy(xpath="//span[contains(text(),'My Data')]")
	public WebElement MyDataTab;
	
	@FindBy(xpath = "//span[contains(text(),'Your Plan Summary')]")
	//span[contains(text(),'Action Needed')]
	public WebElement PlanSummaryTab;*/
	
	
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(1) > a]")
	public WebElement OverviewTab;

	// @FindBy(xpath = "//span[text()='Estimates']")
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(2) > a")
	public WebElement EstimatesTab;

	// @FindBy(xpath = "//span[text()='My Data']")
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(3) > a")
	public WebElement MyDataTab;
	
	@FindBy(css = "div[class='tabs-ma-2 ng-scope'] > ul > li:nth-child(4) > a")
	public WebElement MyElectionsTab;

	@FindBy(xpath = "//span[contains(text(),'Your Plan Summary')]")
	public WebElement YourPlanSummaryLabel;

	@FindBy(xpath = "//a[text()='Estimate Your Pension']")
	public WebElement EstimateYourPensionLink;

	@FindBy(xpath = "//span[contains(text(),'Estimate Your Pension')]")
	public WebElement EstimateYourPensionLabel;

	@FindBy(xpath = "//a[text()='Most Recent Estimate']")
	public WebElement MostRecentEstimateLink;

	@FindBy(xpath = "//a[text()='Saved Estimates']")
	public WebElement SavedEstimatesLink;

	@FindBy(xpath = "//span[contains(text(),'Saved Estimates')]")
	public WebElement SavedEstimatesLabel;

	@FindBy(xpath = "//h2[contains(text(),'My Data')]")
	public WebElement MyDataLabel;
	
	@FindBy(xpath = "//h2[contains(text(),'My Elections')]")
	public WebElement MyElectionsLabel;
		
	@FindBy(xpath="//span[contains(text(),'Normal Retirement')]")
	public WebElement NormalRetirementLabel;

	@FindBy(xpath="(//i[@class='fa fa-stack-1x fa-lg fa-circle-o'])[2]")
	public WebElement Normalretirementradiobutton;
	
	@FindBy(xpath="//h4[contains(text(),'Beneficiary for Joint & Survivor Payments')])")
	public WebElement BeneficiariesLabel;
		
	@FindBy(xpath="(//i[@class='fa fa-stack-1x fa-circle-o'])[1]") //--if Single not selected already
	public WebElement Singlemaritalstatusselect;
	
	@FindBy(xpath="(//i[@class='fa fa-stack-1x fa-circle'])")
	public WebElement Singlemaritalstatus;
		
	@FindBy(xpath="//label[contains(text(),'Single')]")
	public WebElement Singlelabel;
	
	@FindBy(xpath="(//input[@name='dtPickerCurrentDate'])[3]")
	public WebElement BeneficiaryDOB;
	
	@FindBy(xpath="//span[contains(text(),'Continue')]")
	public WebElement Continuebutton;
	
	@FindBy(xpath="//h2[contains(text(),'Please Read This Important Information')]")
	public WebElement AcknowledgementHeader;
	
	@FindBy(xpath="//span[contains(text(),'I understand that this is an estimate. Please calculate my Pension Estimate.')]")
	public WebElement Acknowledgementcheckbox;
	
	@FindBy(xpath="//button[contains(text(),'Accept')]")
	public WebElement Acceptbutton;
	
	@FindBy(xpath="//span[contains(text(),'Calculating Your Estimate')]")
	public WebElement calculateestimatelabel;
	
	@FindBy(xpath="//span[contains(text(),'Your Estimate Has Been Calculated')]")
	public WebElement EstimateCalculatedLabel;
	
	@FindBy(xpath="//h2[contains(text(),'Calculation Error')]")
	public WebElement EstimateCalculatedErrorLabel;
	
	@FindBy(xpath="//button[contains(text(),'View Results')]")
	public WebElement Viewresultsbutton;
	
	@FindBy(xpath="//h2[contains(text(),'Estimate Details')]")
	public WebElement Estimatedetailslabel;
	
	@FindBy(xpath="//h5[contains(text(),'Single Life Annuity')]")
	public WebElement SLALabel;
	
	@FindBy(xpath="//h3[contains(text(),'Assumptions Used for Displayed Pension Estimates')]")
	public WebElement AssumptionLabel;
	
	@FindBy(xpath="//span[contains(text(),'Your Detailed Report')]")
	public WebElement Detailedreportlink;
	
	@FindBy(xpath="//a[contains(text(),'View Assumptions')]")
	public WebElement Viewassumption;
	
	@FindBy(xpath="//span[contains(text(),'Calculate Another Estimate')]")
	public WebElement calculateAnotherestimatebutton;
	
	
	public void Estimatecalc(ExtentTest test)  throws InterruptedException
	{
		clickElement(Normalretirementradiobutton);
		//waitForElementToDisplay(Singlemaritalstatus);
		if(Singlemaritalstatus.isSelected()) {
			BeneficiaryDOB.sendKeys("02/10/1990");	
		} 
		else {
			clickElement(Singlemaritalstatusselect);
			BeneficiaryDOB.sendKeys("02/10/1990");
		}
		//BeneficiaryDOB.sendKeys("02/10/1990");
		clickElement(Continuebutton);
		verifyElementTextContains(AcknowledgementHeader,"Please Read This Important Information",test);
		clickElement(Acknowledgementcheckbox);
		clickElement(Acceptbutton);
		waitForElementToDisplay(calculateestimatelabel);
		verifyElementTextContains(calculateestimatelabel,"Calculating Your Estimate",test);
		waitForElementToDisappear(By.xpath("//span[contains(text(),'Calculating Your Estimate')]"));
		if (EstimateCalculatedLabel.getText().equals("Your Estimate Has Been Calculated"))
		{
			waitForElementToDisplay(Viewresultsbutton);
			clickElement(Viewresultsbutton);
			verifyElementTextContains(Estimatedetailslabel,"Estimate Details",test);
			verifyElementTextContains(SLALabel,"Single Life Annuity",test);
			verifyElementTextContains(AssumptionLabel,"Assumptions Used for Displayed Pension Estimates",test);
			verifyElementTextContains(calculateAnotherestimatebutton,"Calculate Another Estimate",test);
			
		}
		else if (EstimateCalculatedErrorLabel.getText().equals("Calculation Error")){
			waitForElementToDisplay(EstimateCalculatedErrorLabel);
			verifyElementTextContains(EstimateCalculatedErrorLabel,"Calculation Error",test);
			System.out.println("Estimate Calc Failed");
		}
		
	}
	public void EstimatecalcCSO(ExtentTest test)  throws InterruptedException
	{
		clickElement(Normalretirementradiobutton);
		//waitForElementToDisplay(Singlemaritalstatus);
		BeneficiaryDOB.sendKeys("02/10/1990");
		clickElement(Continuebutton);
		verifyElementTextContains(AcknowledgementHeader,"Please Read This Important Information",test);
		clickElement(Acknowledgementcheckbox);
		clickElement(Acceptbutton);
		waitForElementToDisplay(calculateestimatelabel);
		verifyElementTextContains(calculateestimatelabel,"Calculating Your Estimate",test);
		waitForElementToDisappear(By.xpath("//span[contains(text(),'Calculating Your Estimate')]"));
		if (EstimateCalculatedLabel.getText().equals("Your Estimate Has Been Calculated"))
		{
			waitForElementToDisplay(Viewresultsbutton);
			clickElement(Viewresultsbutton);
			verifyElementTextContains(Estimatedetailslabel,"Estimate Details",test);
			verifyElementTextContains(SLALabel,"Single Life Annuity",test);
			verifyElementTextContains(AssumptionLabel,"Assumptions Used for Displayed Pension Estimates",test);
			verifyElementTextContains(calculateAnotherestimatebutton,"Calculate Another Estimate",test);
			
		}
		else if (EstimateCalculatedErrorLabel.getText().equals("Calculation Error")){
			waitForElementToDisplay(EstimateCalculatedErrorLabel);
			verifyElementTextContains(EstimateCalculatedErrorLabel,"Calculation Error",test);
			System.out.println("Estimate Calc Failed");
		}
		
	}
		

}
