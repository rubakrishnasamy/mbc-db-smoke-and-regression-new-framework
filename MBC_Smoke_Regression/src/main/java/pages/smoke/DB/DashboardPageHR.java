package pages.smoke.DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashboardPageHR {
	WebDriver driver;

	public DashboardPageHR(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver = driver;
	}

	@FindBy(xpath = "(//span[contains(text(),'RETIREMENT PENSION')])[1]")
	public WebElement RetirementPensiontitle;

	@FindBy(xpath = "(//a[@class='toplevelLink'])[2]")
	public WebElement BenefitCommencementTile;

	@FindBy(xpath = "(//span[contains(text(),'RETIREMENT PLANNING')])[1]")
	public WebElement RetirementPlanningTile;

	@FindBy(xpath = "//span[contains(text(),'Menu')]")
	public WebElement MenuTab;

	@FindBy(xpath = "//h6[contains(text(),'My Account')]")
	public WebElement MyAccountheader;

	@FindBy(xpath = "//span[contains(text(),'Profile')]")
	public WebElement ProfileLink;

	@FindBy(xpath = "//span[contains(text(),'Communications')]")
	public WebElement CommunicationsLink;

	@FindBy(xpath = "//span[contains(text(),'Beneficiaries')]")
	public WebElement BeneficiariesLink;

	@FindBy(xpath = "//h6[contains(text(),'Retirement Pension')]")
	public WebElement RetirementPensionHeader;

	@FindBy(xpath = "//h6[contains(text(),'Forms & Documents')]")
	public WebElement FormsDocumentsHeader;

	@FindBy(xpath = "//h6[contains(text(),'Knowledge Center')]")
	public WebElement KnowledgeCenterHeader;
	
	@FindBy(xpath = "//a[contains(text(),'Logout')]")
	public WebElement logoutBtn;

	// CIT Plans
	@FindBy(xpath = "//span[contains(text(),'Covestro Pension Plan')]")
	public WebElement CovestroPensionPlanHeader;

	@FindBy(xpath = "//span[contains(text(),'Covestro Supplemental Retirement Plan')]")
	public WebElement CovestroSupplementalRetirementPlanHeader;

	@FindBy(xpath = "//span[contains(text(),'Retirement')]")
	public WebElement RetirementLink;

	@FindBy(xpath = "//span[contains(text(),'Money Management')]")
	public WebElement MoneyManagementLink;

	@FindBy(xpath = "//h6[contains(text(),'UPLOAD DOCUMENTS')]")
	public WebElement UploadDocsLabel;

	@FindBy(xpath = "//span[contains(text(),'General Documents')]")
	public WebElement GeneralDocumentsLink;
	
	//Inpay
	@FindBy(xpath = "//h6[contains(text(),'Pension Payments')]")
	public WebElement Pensionpaymentheader;
	
	@FindBy(xpath = "//span[contains(text(),'Overview')]")
	public WebElement OverviewLink_hm;
	
	@FindBy(xpath = "//span[contains(text(),'Tax Withholding')]")
	public WebElement TaxWithHoldinglink_hm;
	
	@FindBy(xpath = "//span[contains(text(),'Payment Method')]")
	public WebElement PaymentMethodlink_hm;

	// CSO & PROD Plans

	@FindBy(xpath = "//span[contains(text(),'Bayer Corporation Pension Plan')]")
	public WebElement BayerPensionPlanHeader;

	@FindBy(xpath = "//span[contains(text(),'Bayer Corp Supplemental Retirement Plan')]")
	public WebElement BayerSupplementalRetirementPlanHeader;

}
