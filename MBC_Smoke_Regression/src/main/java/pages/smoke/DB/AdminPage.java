package pages.smoke.DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AdminPage {
	WebDriver driver;
	public AdminPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	
	@FindBy(xpath = "(//a[@class='toplevelLink'])[6]")
	public WebElement adminTab;
	
	@FindBy(css="span[class*='pageHead']")
	public WebElement headerTxt;
	
	@FindBy(css="li[id*='nav2-plans']")
	public WebElement planSetupTab;
	
	@FindBy(css="li[id*='nav2-funds']")
	public WebElement fundsTab;
	
	@FindBy(css="li[id*='userinformation']")
	public WebElement usersTab;
	
	@FindBy(css="li[id*='files']")
	public WebElement filesTab;
	
	@FindBy(css="li[id*='nav2-rerun']")
	public WebElement rerunTab;
	
	@FindBy(css="li[id*='systemadministration']")
	public WebElement systemAdminTab;
		
	@FindBy(css="li[id*='reuploadbarradata']")
	public WebElement reuploadTab;
	
	
	
}
