package pages.regression.DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SingleSchemePage {
	WebDriver driver;
	public SingleSchemePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	
	@FindBy(xpath = "(//a[@class='toplevelLink'])[3]")
	public WebElement schemeTab;
	
	@FindBy(css="span[class*='pageHead']")
	public WebElement headerTxt;
	
	@FindBy(id = "nav2-summary")
	public WebElement summaryTab;
	
	@FindBy(id = "nav2-liabilityresults")
	public WebElement liabilityTab;
	
	@FindBy(id = "nav2-assets")
	public WebElement assetsTab;
	
	@FindBy(id = "nav2-fundedstatus")
	public WebElement fundedTab;
	
	
	@FindBy(id = "nav2-cashflow")
	public WebElement cashflowTab;
	
	@FindBy(id = "nav2-statements")
	public WebElement statementTab;
	
	@FindBy(id = "nav2-interestrates")
	public WebElement interestTab;
	
	
}
