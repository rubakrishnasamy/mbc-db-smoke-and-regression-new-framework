package pages.regression.DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DashboardPage {
	WebDriver driver;
	public DashboardPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	@FindBy(css="div[class*='desktop'] a.toplevelLink")
	public WebElement dashboardLink;
	
	@FindBy(xpath="(//a[@class='toplevelLink'])[2]")
	public WebElement dashboardTab;
	
	@FindBy(css="span[class*='pageHead']")
	public WebElement headerTxt;
	
	@FindBy(xpath = "(//li[@id='nav2-actionneeded']/a)")//span[contains(text(),'Action Needed')]
	public WebElement summaryTab;
	
	
	
}
