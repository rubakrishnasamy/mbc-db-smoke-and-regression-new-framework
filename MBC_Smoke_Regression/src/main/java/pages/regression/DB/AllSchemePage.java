package pages.regression.DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AllSchemePage {
	WebDriver driver;
	public AllSchemePage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	
	@FindBy(xpath = "(//a[@class='toplevelLink'])[4]")
	public WebElement allPlanTab;
	
	@FindBy(css="span[class*='pageHead']")
	public WebElement headerTxt;
	
	@FindBy(css="li[id*='nav2-yieldcurve']")
	public WebElement yieldTab;
	
	@FindBy(css="li[id*='prices']")
	public WebElement pricesTab;
	
	
	@FindBy(css="li[id*='alertsandmessagesallplans']")
	public WebElement alertsTab;
	
	@FindBy(css="li[id*='allplansetup']")
	public WebElement setupTab;
	
	
	
}
