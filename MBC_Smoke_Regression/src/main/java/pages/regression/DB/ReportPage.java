package pages.regression.DB;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ReportPage {
	WebDriver driver;
	public ReportPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	
	@FindBy(xpath = "(//a[@class='toplevelLink'])[5]")
	public WebElement reportTab;
	
	@FindBy(css="span[class*='pageHead']")
	public WebElement headerTxt;
	
	
}
