package pages.regression.DB;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	WebDriver driver;
	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
		this.driver=driver;
	}
	@FindBy(css="input[id*='tbUserName']")
	public WebElement userName;
	
	@FindBy(css="span[id*='lblUserName'] label")
	public WebElement userLabel;
	
	@FindBy(css="input[id*='tbPassword']")
	public WebElement password;
	
	@FindBy(css="span[id*='lblPassword'] label")
	public WebElement passwordLabel;
	
	@FindBy(css="input[id*='btnLogin']")
	public WebElement loginBtn;
	
	@FindBy(xpath="//a[contains(text(),'Forgot')]")
	public WebElement forgotTxt;
	
	@FindBy(css="a[id*='topLogout']")
	public WebElement logoutBtn;
	
	public void loginIRL(String username) throws InterruptedException
	{
		
		setInput(userName,username);
		clickElement(loginBtn);
	}
}
