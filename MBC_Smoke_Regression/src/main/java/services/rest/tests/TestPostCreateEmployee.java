package services.rest.tests;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;


import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import utilities.InitTests;


import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyContains;
import static utilities.RestAPI.postWithResponse;
import static utilities.RestAPI.get;
import static verify.SoftAssertions.verifyEquals;
public class TestPostCreateEmployee extends InitTests
{
	
	@Test(priority=1)
	public static void testPostCreateProject() throws IOException
	{
		ExtentTest test=null;

		try {
			test = reports.createTest("testPostCreateProject");
			test.assignCategory("REST");
			String path = dir_path+"\\src\\main\\resources\\testdata\\create.json";
			 HashMap<String, String> headers = new HashMap<String, String>();
			
				headers.put("Content-Type","application/json");			
				File file;
			
			file = new File(path);
			Response response = postWithResponse("http://dummy.restapiexample.com/api/v1/create",file,null,headers);
			System.out.println("message body in testPostCreateProject() " + response.getBody().asString());
			System.out.println("status code " + response.getStatusCode());
			verifyEquals(response.getStatusCode(),200, test);
			 test.log(Status.INFO, "response body"+response.getBody().asString());

			verifyContains(response.getBody().asString(), "id","created successfully",test);
			 JsonPath jsonPathEvaluator = response.jsonPath();
				verifyEquals(jsonPathEvaluator.get("name"), "TestEmployee5", test);

			 
		} catch (Error e) {
			test.log(Status.FAIL, "testPostCreateProject() "+"actual " +e.getMessage() );
			softAssert.assertAll();

			e.printStackTrace();

		} catch (Exception e) {
			test.log(Status.FAIL, "testPostCreateProject() "+"actual " +e.getMessage() );
			softAssert.assertAll();

			e.printStackTrace();
		} 
		finally
		{
			reports.flush();
		
		}
		
		
	
	}

	

	
}
