package services.rest.tests;
import static utilities.MyExtentReports.reports;
import static utilities.RestAPI.get;
import static verify.SoftAssertions.verifyEquals;
import static verify.SoftAssertions.verifyContains;

import java.io.IOException;
import java.util.HashMap;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;

import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import utilities.InitTests;
public class TestGetListOfEmployees extends InitTests
{
	
	@Test()
	public static void testGetListOfEmployees() throws IOException
	{
		ExtentTest test=null;
		try {
			
			test = reports.createTest("testGetListOfEmployees");
			test.assignCategory("REST");
			 HashMap<String, String> headers = new HashMap<String, String>();
			//headers.put("", "");
			headers.put("Accept","application/json");
			Response response = get("http://dummy.restapiexample.com/api/v1/employees",null,null,headers);
			System.out.println("message body " + response.getBody().asString());
			System.out.println("status code " + response.getStatusCode());
			verifyEquals(response.getStatusCode(), 200, test);
			String responseJson=response.body().asString();
			verifyContains(responseJson, "employee_name","got expected key", test);
			verifyContains(responseJson, "id","got expected key", test);
			verifyContains(responseJson, "employee_age","got expected key", test);
			verifyContains(responseJson, "employee_salary","got expected key", test);


			
		 JsonPath jsonPathEvaluator = response.jsonPath();
		 test.log(Status.INFO, "employee_name[4] "+jsonPathEvaluator.get("employee_name[4]"));
			test.log(Status.INFO, "id[4] "+jsonPathEvaluator.get("id[4]"));
			test.log(Status.INFO, "employee_age[4] "+jsonPathEvaluator.get("employee_age[4]"));

			 verifyEquals(jsonPathEvaluator.get("employee_name[1]"),"invalid name","Got expected employee name",test);
			 verifyEquals(jsonPathEvaluator.get("id[1]"),"0","Got expected employee id",test);
			 verifyEquals(jsonPathEvaluator.get("employee_age[1]"),"0","Got expected employee age",test);

			verifyEquals(jsonPathEvaluator.get("employee_name[4]"),"INVALID","Got un expected employee",test);

		 verifyEquals(jsonPathEvaluator.get("id[4]"),"0","Got un expected employee",test);
			 verifyEquals(jsonPathEvaluator.get("employee_age[4]"),"0","Got un expected employee",test);


		} catch (Error e) {
			e.printStackTrace();

		} catch (Exception e) {
			e.printStackTrace();
		} 
		finally
		{
			reports.flush();
		
		}
		
		
	
	}

	
}
