package smoke.test.DB;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.smoke.DB.DashboardPageLR;
import pages.smoke.DB.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_QAIMP extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_QAIMP(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_QAIMP qaimp = new Verify_QAIMP("QAIMP");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true, priority = 1)
	public void verifyQAIMP() throws Exception {

		try {
			test = reports.createTest("verifyQAIMP_PageNavigations");
			test.assignCategory("smoke");

			driver = driverFact.getEventDriver(webdriver, test);

			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.userName);
			verifyElementTextContains(login.userLabel, "username", test);
			verifyElementTextContains(login.passwordLabel, "password", test);
			verifyElementTextContains(login.forgotTxt, "Forgot username or password?", test);
			login.loginDB(USERNAME, PASSWORD);

			DashboardPageLR dashboard = new DashboardPageLR(driver);
			waitForElementToDisplay(dashboard.DashboardTab);
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			// waitForElementToDisplay(dashboard.BenefitCommencementTile);
			verifyElementTextContains(dashboard.BenefitCommencementTile, "Benefit Commencement", test);
			waitForElementToDisplay(dashboard.MPEstimatorTile);
			verifyElementTextContains(dashboard.MPEstimatorTile, "Multi-Plan Estimator Tool", test);

			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);

			scrollToElement(driver, dashboard.MenuBtn);
			// verifyElementText(dashboard.MenuBtn, "Menu", test);
			clickElement(dashboard.MenuBtn);

			waitForElementToDisplay(dashboard.HomeBtn);
			// verifyElementText(dashboard.HomeBtn, "Home", test);
			waitForElementToDisplay(dashboard.MyAccountLabel);
			verifyElementTextContains(dashboard.MyAccountLabel, "My Account", test);
			verifyElementTextContains(dashboard.ProfileLink, "Profile", test);
			//verifyElementTextContains(dashboard.CommunicationsLink, "Communication", test);
			verifyElementTextContains(dashboard.BeneficiariesLink, "Beneficiaries", test);
			verifyElementTextContains(dashboard.Forms_DocLabel, "Forms & Documents", test);
			verifyElementTextContains(dashboard.Retirement_PensionLink, "Retirement Pension", test);
			verifyElementTextContains(dashboard.KnowledgeCenterLabel, "Knowledge Center", test);
			verifyElementTextContains(dashboard.RetirementLink, "Retirement", test);
			verifyElementTextContains(dashboard.MoneyManagementLink, "Money Management", test);
			verifyElementTextContains(dashboard.UploadDocsLabel, "UPLOAD DOCUMENTS", test);
			verifyElementTextContains(dashboard.GeneralDocumentsLink, "General Documents", test);

			clickElement(dashboard.HomeBtn);

			waitForElementToDisplay(dashboard.DashboardTab);
			clickElement(dashboard.DashboardTab);
			waitForElementToDisplay(dashboard.MPEstimatorTile);
			verifyElementTextContains(dashboard.MPEstimatorTile, "Multi-Plan Estimator Tool", test);
			verifyElementTextContains(dashboard.EstimateNowBtn, "ESTIMATE NOW", test);
			clickElement(dashboard.EstimateNowBtn);

			//String url = "https://ben-qai.mercerbenefitscentral.com/MyView";
			//switchToWindowWithURL(url, driver);
			clickElement(dashboard.PopOKBtn);
			
			verifyElementTextContains(dashboard.EstYourMPPensionBenTab, "Estimate Your Multi-Plan Pension Benefits", test);
			waitForElementToDisplay(dashboard.EstYourMPPensionBenLabel);
			verifyElementTextContains(dashboard.EstYourMPPensionBenLabel, "Estimate Your Multi-Plan Pension Benefits", test);
			
			clickElement(dashboard.MstRcntMPEstTab);
			waitForElementToDisplay(dashboard.MPEstResultsLabel);
			verifyElementTextContains(dashboard.MPEstResultsLabel, "Multi-Plan Estimate Results", test);
			
			clickElement(dashboard.SavedMPEstTab);
			waitForElementToDisplay(dashboard.SavedMPEstLabel);
			verifyElementTextContains(dashboard.SavedMPEstLabel, "Saved Multi-Plan Estimates", test);

			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyQAIMP()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyQAIMP()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}
