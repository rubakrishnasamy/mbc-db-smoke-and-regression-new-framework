package smoke.test.DB;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.smoke.DB.*;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_CSOIP extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_CSOIP(String appName) {
		super(appName);
	}

	@Test(enabled = true)
	public void verifyCSOIP() throws Exception {
		new Verify_CSOIP("CSOIP");
		try {
			test = reports.createTest("verifyPageNavigations_CSOIP");
			test.assignCategory("smoke");

			webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
			driver = driverFact.getEventDriver(webdriver, test);

			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.userName);
			verifyElementTextContains(login.userLabel, "username", test);
			verifyElementTextContains(login.passwordLabel, "password", test);
			verifyElementTextContains(login.forgotTxt, "Forgot username or password?", test);
			login.loginDB(USERNAME, PASSWORD);

			MfaPage mfa = new MfaPage(driver);
			waitForElementToDisplay(mfa.mfapageheader);
			System.out.println(driver.getTitle());
			mfa.authenticate();

			DashboardPageHR dashboard = new DashboardPageHR(driver);
			waitForElementToDisplay(dashboard.RetirementPensiontitle);

			// verifyElementTextContains(dashboard.BenefitCommencementTile,"Benefit
			// Commencement",test);
			verifyElementTextContains(dashboard.RetirementPlanningTile, "RETIREMENT PLANNING", test);
			verifyElementTextContains(dashboard.MenuTab, "Menu", test);

			clickElement(dashboard.MenuTab);
			// System.out.println(dashboard.dashboardTab.getText());
			waitForElementToDisplay(dashboard.MyAccountheader);
			verifyElementTextContains(dashboard.MyAccountheader, "My Account", test);
			verifyElementTextContains(dashboard.ProfileLink, "Profile", test);
			verifyElementTextContains(dashboard.CommunicationsLink, "Communication", test);
			verifyElementTextContains(dashboard.BeneficiariesLink, "Beneficiaries", test);
			verifyElementTextContains(dashboard.FormsDocumentsHeader, "Forms & Documents", test);
			verifyElementTextContains(dashboard.KnowledgeCenterHeader, "Knowledge Center", test);
			verifyElementTextContains(dashboard.RetirementLink, "Retirement", test);
			verifyElementTextContains(dashboard.MoneyManagementLink, "Money Management", test);
			verifyElementTextContains(dashboard.UploadDocsLabel, "UPLOAD DOCUMENTS", test);
			verifyElementTextContains(dashboard.GeneralDocumentsLink, "General Documents", test);
			verifyElementTextContains(dashboard.Pensionpaymentheader, "Pension Payments", test);
			verifyElementTextContains(dashboard.OverviewLink_hm, "Overview", test);
			verifyElementTextContains(dashboard.TaxWithHoldinglink_hm, "Tax Withholding", test);
			verifyElementTextContains(dashboard.PaymentMethodlink_hm, "Payment Method", test);
			clickElement(dashboard.OverviewLink_hm);

			InPayPage Inpaymp = new InPayPage(driver);
			waitForElementToDisplay(Inpaymp.PaymentSummaryLabel);
			verifyElementTextContains(Inpaymp.PaymentSummaryLabel, "Payment Summary", test);
			clickElement(Inpaymp.PaymentHistoryLink);
			waitForElementToDisplay(Inpaymp.PaymentHistoryLabel);
			verifyElementTextContains(Inpaymp.PaymentHistoryLabel, "Payment History", test);
			clickElement(Inpaymp.ScheduledDeductionsLink);
			waitForElementToDisplay(Inpaymp.ScheduledDeductionsLabel);
			verifyElementTextContains(Inpaymp.ScheduledDeductionsLabel, "Scheduled Deductions", test);

			clickElement(Inpaymp.TaxWithHoldingTab);
			waitForElementToDisplay(Inpaymp.FederalTaxWithHoldingLabel);
			verifyElementTextContains(Inpaymp.FederalTaxWithHoldingLabel, "Federal Tax Withholding", test);
			clickElement(Inpaymp.StateLink);
			waitForElementToDisplay(Inpaymp.StateTaxWithHoldingLabel);
			verifyElementTextContains(Inpaymp.StateTaxWithHoldingLabel, "State Tax Withholding", test);

			clickElement(Inpaymp.PaymentMethodTab);
			waitForElementToDisplay(Inpaymp.PaymentMethodLabel);
			verifyElementTextContains(Inpaymp.PaymentMethodLabel, "Payment Method", test);

			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			clickElement(dashboard.logoutBtn);
			waitForElementToDisplay(login.userLabel);
			verifyElementTextContains(login.userLabel, "username", test);

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyQAIIMP()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyQAIIMP()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}
