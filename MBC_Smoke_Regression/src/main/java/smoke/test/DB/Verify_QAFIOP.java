package smoke.test.DB;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.smoke.DB.DashboardPageLR;
import pages.smoke.DB.InPayPage;
import pages.smoke.DB.LoginPage;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_QAFIOP extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_QAFIOP(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_QAFIOP qafiop = new Verify_QAFIOP("QAFIOP");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true)
	public void verifyQAFIOP() throws Exception {

		try {
			test = reports.createTest("verifyQAFIOP_PageNavigations");
			test.assignCategory("smoke");

			driver = driverFact.getEventDriver(webdriver, test);

			DashboardPageLR dashboard = new DashboardPageLR(driver);
			waitForElementToDisplay(dashboard.DashboardTab);
			///verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			verifyElementTextContains(dashboard.RetirementPlanningTile, "RETIREMENT PLANNING", test);
			
			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);

			scrollToElement(driver, dashboard.MenuBtn);
			// verifyElementText(dashboard.MenuBtn, "Menu", test);
			clickElement(dashboard.MenuBtn);

			waitForElementToDisplay(dashboard.HomeBtn);
			// verifyElementText(dashboard.HomeBtn, "Home", test);
			waitForElementToDisplay(dashboard.MyAccountLabel);
			verifyElementTextContains(dashboard.MyAccountLabel, "My Account", test);
			verifyElementTextContains(dashboard.ProfileLink, "Profile", test);
			verifyElementTextContains(dashboard.CommunicationsLink, "Communication", test);
			verifyElementTextContains(dashboard.BeneficiariesLink, "Beneficiaries", test);
			verifyElementTextContains(dashboard.Forms_DocLabel, "Forms & Documents", test);
			verifyElementTextContains(dashboard.Retirement_PensionLink, "Retirement Pension", test);
			verifyElementTextContains(dashboard.KnowledgeCenterLabel, "Knowledge Center", test);
			verifyElementTextContains(dashboard.RetirementLink, "Retirement", test);
			verifyElementTextContains(dashboard.MoneyManagementLink, "Money Management", test);
			verifyElementTextContains(dashboard.UploadDocsLabel, "UPLOAD DOCUMENTS", test);
			verifyElementTextContains(dashboard.GeneralDocumentsLink, "General Documents", test);
			verifyElementTextContains(dashboard.TaxWithHoldinglink_hm, "Tax Withholding", test);
			verifyElementTextContains(dashboard.PaymentMethodlink_hm, "Payment Method", test);
			
			clickElement(dashboard.OverviewLink_hm);
			
			InPayPage Inpayop = new InPayPage(driver);
			waitForElementToDisplay(Inpayop.PaymentSummaryLabel);
			verifyElementTextContains(Inpayop.PaymentSummaryLabel, "Payment Summary", test);
			clickElement(Inpayop.PaymentHistoryLink);
			waitForElementToDisplay(Inpayop.PaymentHistoryLabel);
			verifyElementTextContains(Inpayop.PaymentHistoryLabel, "Payment History", test);
			clickElement(Inpayop.ScheduledDeductionsLink);
			waitForElementToDisplay(Inpayop.ScheduledDeductionsLabel);
			verifyElementTextContains(Inpayop.ScheduledDeductionsLabel, "Scheduled Deductions", test);
			
			clickElement(Inpayop.TaxWithHoldingTab);
			waitForElementToDisplay(Inpayop.FederalTaxWithHoldingLabel);
			verifyElementTextContains(Inpayop.FederalTaxWithHoldingLabel, "Federal Tax Withholding", test);
			clickElement(Inpayop.StateLink);
			waitForElementToDisplay(Inpayop.StateTaxWithHoldingLabel);
			verifyElementTextContains(Inpayop.StateTaxWithHoldingLabel, "State Tax Withholding", test);
			
			clickElement(Inpayop.PaymentMethodTab);
			waitForElementToDisplay(Inpayop.PaymentMethodLabel);
			verifyElementTextContains(Inpayop.PaymentMethodLabel, "Payment Method", test);

			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("VerifyIRL()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("VerifyIRL()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}
