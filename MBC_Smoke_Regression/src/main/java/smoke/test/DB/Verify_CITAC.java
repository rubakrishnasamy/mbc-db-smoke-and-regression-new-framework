package smoke.test.DB;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.isElementExisting;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.smoke.DB.*;


import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_CITAC extends InitTests {
	
	Driver driverFact = new Driver();
	WebDriver driver=null;
	WebDriver webdriver = null;
	ExtentTest test=null;
	
	public Verify_CITAC(String appName) {
		super(appName);
	}

	@Test(enabled=true)
	public void verifyCITAC() throws Exception {
		
		new Verify_CITAC("CITAC");
		try {
			test = reports.createTest("verifyCITACPageNavigations");
			test.assignCategory("smoke");
			
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			driver = driverFact.getEventDriver(webdriver, test); 
			
			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.userName);
			verifyElementTextContains(login.userLabel, "username", test);
			verifyElementTextContains(login.passwordLabel, "password", test);
			verifyElementTextContains(login.forgotTxt, "Forgot username or password?", test);
			login.loginDB(USERNAME, PASSWORD);
			
			MfaPage mfa = new MfaPage(driver);
			waitForElementToDisplay(mfa.mfapageheader);
			System.out.println(driver.getTitle());
			mfa.authenticate();
			
			DashboardPageHR dashboard = new DashboardPageHR(driver);
			waitForElementToDisplay(dashboard.RetirementPensiontitle, driver, 500);
			
			//verifyElementTextContains(dashboard.BenefitCommencementTile,"Benefit Commencement",test);
			verifyElementTextContains(dashboard.RetirementPlanningTile,"RETIREMENT PLANNING",test);
			verifyElementTextContains(dashboard.MenuTab,"Menu",test);
			
			clickElement(dashboard.MenuTab);
			//System.out.println(dashboard.dashboardTab.getText());
			waitForElementToDisplay(dashboard.MyAccountheader);
			verifyElementTextContains(dashboard.MyAccountheader, "My Account", test);
			verifyElementTextContains(dashboard.ProfileLink, "Profile", test);
			verifyElementTextContains(dashboard.CommunicationsLink, "Communication", test);
			verifyElementTextContains(dashboard.BeneficiariesLink, "Beneficiaries", test);
			verifyElementTextContains(dashboard.FormsDocumentsHeader, "Forms & Documents", test);
			verifyElementTextContains(dashboard.RetirementPensionHeader, "Retirement Pension", test);
			verifyElementTextContains(dashboard.KnowledgeCenterHeader, "Knowledge Center", test);
			verifyElementTextContains(dashboard.RetirementLink, "Retirement", test);
			verifyElementTextContains(dashboard.MoneyManagementLink, "Money Management", test);
			verifyElementTextContains(dashboard.UploadDocsLabel, "UPLOAD DOCUMENTS", test);
			verifyElementTextContains(dashboard.GeneralDocumentsLink, "General Documents", test);
			verifyElementTextContains(dashboard.CovestroPensionPlanHeader,"Covestro Pension Plan",test);
			verifyElementTextContains(dashboard.CovestroSupplementalRetirementPlanHeader,"Covestro Supplemental Retirement Plan",test);
			clickElement(dashboard.CovestroSupplementalRetirementPlanHeader);
			
			EstimatePage Estimate = new EstimatePage(driver);
			waitForElementToDisplay(Estimate.OverviewHRtab);
			verifyElementTextContains(Estimate.OverviewHRtab,"Overview",test);
			verifyElementTextContains(Estimate.EstimatesTab,"Estimates",test);
			verifyElementTextContains(Estimate.MyDataTab,"My Data",test);
			waitForElementToDisplay(Estimate.YourPlanSummaryLabel);
			verifyElementTextContains(Estimate.YourPlanSummaryLabel,"Your Plan Summary",test);
			
			clickElement(Estimate.EstimatesTab);
			waitForElementToDisplay(Estimate.EstimateYourPensionLabel);
			verifyElementTextContains(Estimate.EstimateYourPensionLabel, "Estimate Your Pension", test);
			waitForElementToDisplay(Estimate.NormalRetirementLabel);
			verifyElementTextContains(Estimate.NormalRetirementLabel, "Normal Retirement", test);
			waitForElementToDisplay(Estimate.Normalretirementradiobutton);
			
			//verifyElementTextContains(Estimate.BeneficiariesLabel,"Beneficiary for Joint & Survivor Payments",test);
		    Estimate.Estimatecalc(test);
		    verifyElementTextContains(dashboard.logoutBtn,"Logout",test);
			clickElement(dashboard.logoutBtn);
			waitForElementToDisplay(login.userLabel);
			verifyElementTextContains(login.userLabel,"username",test);
			
			

		} catch (Error e) {
			e.printStackTrace();     
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("VerifyIRL()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("VerifyIRL()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.close();

		}
	} 

}

