package smoke.test.DB;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.clickElementUsingJavaScript;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementTextContains;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.smoke.DB.DashboardPageLR;
import pages.smoke.DB.EstimatePage;
import pages.smoke.DB.LoginPage;
import pages.smoke.DB.ROL_Page;
import utilities.InitTests;
import verify.SoftAssertions;

public class Verify_BWQAIRK extends InitTests {
	Driver driverFact = new Driver();
	WebDriver driver = null;
	WebDriver webdriver = null;
	ExtentTest test = null;

	public Verify_BWQAIRK(String appName) {
		super(appName);
	}

	@BeforeClass
	public void beforeclass() throws Exception {
		Verify_BWQAIRK bwqairk = new Verify_BWQAIRK("BWQAIRK");
		webdriver = driverFact.initWebDriver(BASEURL, BROWSER_TYPE, "local", "");
	}

	@Test(enabled = true, priority = 1)
	public void verifyBWQAIRK() throws Exception {

		try {

			test = reports.createTest("verifyBWQAIRK_PageNavigations");
			test.assignCategory("smoke");

			driver = driverFact.getEventDriver(webdriver, test);

			LoginPage login = new LoginPage(driver);
			waitForElementToDisplay(login.BWuserName);
			verifyElementTextContains(login.BWLogLabel, "Log in to your existing account.", test);
			verifyElementTextContains(login.BWforgotTxt, "Forgot User Name or Password?", test);
			login.BWloginDB(USERNAME, PASSWORD);

			DashboardPageLR dashboard = new DashboardPageLR(driver);
			waitForElementToDisplay(dashboard.DashboardTab);
			verifyElementTextContains(dashboard.ADPTile, "ADP Voluntary Early Retirement Program", test);
			verifyElementTextContains(dashboard.BenefitCommencementTile, "Benefit Commencement", test);
			verifyElementTextContains(dashboard.RetirementPensionTitle, "RETIREMENT PENSION", test);
			
			clickElement(dashboard.BenefitsSummaryTab);
			waitForElementToDisplay(dashboard.RetirementLabel);
			verifyElementTextContains(dashboard.RetirementLabel, "Retirement", test);
			clickElement(dashboard.DashboardTab);
			
			scrollToElement(driver, dashboard.MenuBtn);
			// verifyElementText(dashboard.MenuBtn, "Menu", test);
			clickElement(dashboard.MenuBtn);

			waitForElementToDisplay(dashboard.HomeBtn);
			// verifyElementText(dashboard.HomeBtn, "Home", test);
			waitForElementToDisplay(dashboard.MyAccountLabel);
			verifyElementTextContains(dashboard.MyAccountLabel, "My Account", test);
			verifyElementTextContains(dashboard.ProfileLink, "Profile", test);
			verifyElementTextContains(dashboard.CommunicationsLink, "Communication", test);
			verifyElementTextContains(dashboard.BeneficiariesLink, "Beneficiaries", test);
			verifyElementTextContains(dashboard.Forms_DocLabel, "Forms & Documents", test);
			verifyElementTextContains(dashboard.Retirement_PensionLink, "Retirement Pension", test);
			verifyElementTextContains(dashboard.KnowledgeCenterLabel, "Knowledge Center", test);
			verifyElementTextContains(dashboard.RetirementLink, "Retirement", test);
			verifyElementTextContains(dashboard.MoneyManagementLink, "Money Management", test);
			
			clickElement(dashboard.HomeBtn);
			waitForElementToDisplay(dashboard.DashboardTab);
			verifyElementTextContains(dashboard.BenefitCommencementTile, "Benefit Commencement", test);
			waitForElementToDisplay(dashboard.GetStartedBtn);
			clickElement(dashboard.GetStartedBtn);

			ROL_Page rolpg = new ROL_Page(driver);
			waitForElementToDisplay(rolpg.BenefitCommencementLabel);
			verifyElementTextContains(rolpg.BenefitCommencementLabel, "Benefit Commencement", test);
			verifyElementTextContains(rolpg.BenefitCommOverviewLabel, "Benefit Commencement", test);
			verifyElementTextContains(rolpg.StatusDetailsLink, "Status Details", test);
			clickElement(rolpg.StatusDetailsLink);
			
			verifyElementTextContains(rolpg.BenCommStepsLabel, "Benefit Commencement Steps", test);
			verifyElementTextContains(rolpg.StepOneLabel, "1", test);
			verifyElementTextContains(rolpg.StepTwoLabel, "2", test);
			verifyElementTextContains(rolpg.StepThreeLabel, "3", test);
			
			clickElement(rolpg.BackOVBtn);
			
			waitForElementToDisplay(rolpg.BenefitCommencementLabel);
			verifyElementTextContains(rolpg.BenefitCommencementLabel, "Benefit Commencement", test);
			verifyElementTextContains(rolpg.BenefitCommOverviewLabel, "Benefit Commencement", test);
			waitForElementToDisplay(rolpg.GetStartedRKBtn);
			verifyElementTextContains(rolpg.GetStartedRKBtn, "Get Started", test);
			clickElement(rolpg.GetStartedRKBtn);
			
			waitForElementToDisplay(rolpg.ReqBenCommKitLabel);
			verifyElementTextContains(rolpg.ReqBenCommKitLabel, "Request a Benefit Commencement Kit", test);

			waitForElementToDisplay(dashboard.logoutBtn);
			verifyElementTextContains(dashboard.logoutBtn, "Logout", test);
			dashboard.logoutDB();

		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyQAIOP()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver, new Exception().getStackTrace()[0].getMethodName()),
					test);
			ATUReports.add("verifyQAIOP()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} finally {
			reports.flush();
			driver.close();
		}
	}

}
